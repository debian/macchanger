# Italian translation of macchanger debconf messages
# Copyright (C) 2014 macchanger package's copyright holder.
# This file is distributed under the same license as the macchanger package.
# Beatrice Torracca <beatricet@libero.it>, 2015.
msgid ""
msgstr ""
"Project-Id-Version: macchanger\n"
"Report-Msgid-Bugs-To: macchanger@packages.debian.org\n"
"POT-Creation-Date: 2014-12-18 13:38+0100\n"
"PO-Revision-Date: 2015-02-14 13:38+0200\n"
"Last-Translator: Beatrice Torracca <beatricet@libero.it>\n"
"Language-Team: Italian <debian-l10n-italian@lists.debian.org>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Virtaal 0.7.1\n"

#. Type: boolean
#. Description
#: ../templates:1001
msgid "Change MAC automatically?"
msgstr "Cambiare MAC automaticamente?"

#. Type: boolean
#. Description
#: ../templates:1001
msgid ""
"Please specify whether macchanger should be set up to run automatically "
"every time a network device is brought up or down. This gives a new MAC "
"address whenever you attach an ethernet cable or reenable wifi."
msgstr ""
"Specificare se macchanger debba essere impostato per l'esecuzione automatica "
"ogni volta che un dispositivo di rete viene attivato o disattivato. Ciò "
"fornisce un nuovo indirizzo MAC ogni volta che si inserisce un cavo Ethernet "
"o si riattiva il Wi-Fi."
